const jwt = require("jsonwebtoken");
const { listen } = require("../app");

const verifyToken = (req, res, next) => {
  let token = req.headers.authorization;
  if (!token) {
    return res.status(401).json({ error: { message: "Unauthorized" } });
  }
  token = token.split(" ")[1];
  jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
    if (err)
      return res.status(401).json({ error: { message: "Invalid token" } });
    req.user = decoded.user;
    next();
  });
};

module.exports = verifyToken;
