const responseTransformer = (req, resp, next) => {
  const original = resp.json;
  resp.json = function (data) {
    if (resp.statusCode < 400) original.call(this, { data });
    else {
      data.statusCode = resp.statusCode;
      original.call(this, { error: data });
    }
  };
  next();
};

module.exports = responseTransformer;
