var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const cors = require("cors");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/user.route");
var customersRouter = require("./routes/customer.route");
var servicesRouter = require("./routes/service.route");
var employeesRouter = require("./routes/employee.route");
var uploadRouter = require("./routes/upload.route");
var appointmentsRouter = require("./routes/appointment.route");
var securityRouter = require("./routes/security.route");
var expenseRouter = require("./routes/expense.route");
var statisticRouter = require("./routes/statistic.route");
const responseTransformer = require("./middleware/response.middleware");
const CronService = require("./service/cron.service");
var specialOfferRouter = require("./routes/special-offer.route");
var staffRouter = require("./routes/staff.route");

require("dotenv").config();
require("./config/db");

var app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "uploads")));
app.use(responseTransformer);

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/customers", customersRouter);
app.use("/services", servicesRouter);
app.use("/employees", employeesRouter);
app.use("/", appointmentsRouter);
app.use("/auth", securityRouter);
app.use("/expenses", expenseRouter);
app.use("/statistics", statisticRouter);
app.use("/special-offers", specialOfferRouter);
app.use("/", uploadRouter);
app.use("/staff", staffRouter);

CronService.hourlyCron();

app.get("/current", (req, res) => {
  res.json({
    time1: new Date(),
    time2: new Date().toUTCString(),
    time3: new Date("2024-02-14T12:00:00"),
    time: new Date("2024-02-14T12:00:00Z"),
  });
});

module.exports = app;
