# Beauty backend

The webservice part of beauty web application

## Prerequistes

- Node
- For dev environment
  - Docker
  - Docker compose

## How to run localy

1.  Run MongoDB

```
docker compose up -d
```

2. Install all dependencies

```
npm i or yarn i
```

3. Run dev server

```
npm run dev
```

## Note

- You can manage mongo database in `localhost:8081`

- To run fixture and refresh data

```
npm run fixture
```

## Running test

```
# Unit test
npm run test:unit

# Integration test
npm run test

# Api test
npm run test:api
```
