var express = require("express");
const User = require("../models/user.model");
const verifyToken = require("../middleware/authenticator.middleware");
var router = express.Router();

router.put("/:id", verifyToken, async (req, res) => {
  const currentUser = await User.findById(req.user._id);
  const updateUser = req.body;
  currentUser.username = updateUser.username;
  currentUser.firstName = updateUser.firstName;
  currentUser.lastName = updateUser.lastName;
  currentUser.photo = updateUser.photo;
  currentUser.preferences = updateUser.preferences;
  await currentUser.save();
  res.json({ message: "OK" });
});

module.exports = router;
