var express = require("express");
const Service = require("../models/service.model");
var router = express.Router();

router.get("/", async function (req, res) {
  let query = {};
  query.isDeleted = false;
  if (req.query.name) {
    query.name = {
      $regex: new RegExp(req.query.name, "i"),
    };
  }
  if (req.query.priceMin) {
    query["price.amount"] = {
      $gte: req.query.priceMin,
    };
  }
  if (req.query.priceMax) {
    query["price.amount"] = {
      ...query["price.amount"],
      $lte: req.query.priceMax,
    };
  }
  if (req.query.durationMin) {
    query.duration = {
      $gte: req.query.durationMin,
    };
  }
  if (req.query.durationMax) {
    query.duration = {
      ...queryduration,
      $lte: req.query.durationMax,
    };
  }
  res.json(await Service.find(query).populate("promotion").exec());
});

router.post("/", async (req, res) => {
  const newService = new Service(req.body);
  await newService.save();
  res.statusCode = 201;
  res.json({ message: "OK" });
});

router.put("/:id", async (req, res) => {
  await Service.updateOne({ _id: req.params.id }, req.body);
  res.json({ message: "OK" });
});

router.get("/:id", async (req, res) => {
  const service = await Service.findById(req.params.id);
  res.json(service);
});

router.delete("/:id", async (req, res) => {
  const service = await Service.findById(req.params.id);
  service.isDeleted = true;
  service.save();
  res.json({ message: "OK" });
});

module.exports = router;
