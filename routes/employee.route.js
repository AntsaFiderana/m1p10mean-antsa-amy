var express = require("express");
const Employee = require("../models/employee.model");
const EmployeeService = require("../service/employee.service");
const { currentDate } = require("../utils/date.utils");
var router = express.Router();

router.post("/available", async (req, res) => {
  const times = req.body;
  const availableEmployees = [];
  for (const slot of times.slots) {
    slot.startTime = new Date(slot.startTime);
    slot.endTime = new Date(slot.endTime);
    availableEmployees.push({
      _id: slot._id,
      employees: await Employee.findAvailable({ date: times.date, ...slot }),
    });
  }
  res.json(availableEmployees);
});

router.put("/:id/working-hours", async (req, res) => {
  const employee = await Employee.findById(req.params.id);
  employee.workingHours = req.body;
  await employee.save();
  res.json({ message: "OK" });
});

router.post("/", async (req, res) => {
  const newEmployee = new Employee(req.body);
  await newEmployee.save();
  res.statusCode = 201;
  res.json({ message: "OK" });
});

router.put("/:id", async (req, res) => {
  await Employee.updateOne({ _id: req.params.id }, req.body);
  res.json({ message: "OK" });
});

router.get("/:id", async (req, res) => {
  const employee = await Employee.findById(req.params.id);
  res.json(employee);
});

router.get("/", async (req, res) => {
  const employees = await Employee.find({ isDeleted: false }).exec();
  res.json(employees);
});

router.get("/:id/tasks", async (req, res) => {
  const tasks = await EmployeeService.getDailyTasksByEmployee(
    req.params.id,
    req.query.date ?? new Date()
  );
  res.json(tasks);
});

router.delete("/:id", async (req, res) => {
  const employee = await Employee.findById(req.params.id);
  employee.isDeleted = true;
  employee.save();
  res.json({ message: "OK" });
});

module.exports = router;
