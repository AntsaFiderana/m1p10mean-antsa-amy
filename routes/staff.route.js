var express = require("express");
var router = express.Router();
const User = require("../models/user.model");
const EmployeeService = require("../service/employee.service");
const { handle } = require("../error/validation.error");

router.get("/", async (req, res) => {
  const staff = await User.find({
    isDeleted: false,
    roles: { $in: ["EMP", "ADMIN"] },
  });
  res.json(staff);
});

router.post("/", async (req, res) => {
  try {
    await EmployeeService.createStaff(req.body);
    res.statusCode = 201;
    res.json({ message: "OK" });
  } catch (error) {
    handle(error, res);
  }
});

router.put("/:id", async (req, res) => {
  try {
    await User.updateOne({ _id: req.params.id }, req.body);
    res.json({ message: "OK" });
  } catch (error) {
    handle(error);
  }
});

router.get("/:id", async (req, res) => {
  const staff = await User.findById(req.params.id);
  res.json(staff);
});

module.exports = router;
