var express = require("express");
const SpecialOffer = require("../models/special-offer.model");
const SpecialOfferService = require("../service/special-offer.service");
const http = require("http");

var router = express.Router();

router.get("/", async function (req, res) {
  res.json(await SpecialOffer.find());
});

router.post("/", async (req, res) => {
  try {
    const { service_id, startDate, endDate, price } = req.body;
    const newSpecialOffer = await SpecialOfferService.createSpecialOffer(
      service_id,
      startDate,
      endDate,
      price
    );
    await SpecialOfferService.specialOffersNotification(newSpecialOffer);
    res.statusCode = 201;
    res.json({ message: "OK" });
  } catch (error) {
    console.error("Error creating special offer:", error);
    res.status(500).json({ error: "Internal server error." });
  }
});

router.get("/current", async (req, res) => {
  const specialOffers = await SpecialOfferService.getCurrentSpecialOffers();
  res.json(specialOffers);
});

module.exports = router;
