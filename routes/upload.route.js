var express = require("express");
const multer = require("multer");
var router = express.Router();
const path = require("path");
const { v4 } = require("uuid");

const uploadDir = path.join(__dirname, "../uploads");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadDir);
  },
  filename: function (req, file, cb) {
    const v = v4();
    cb(null, `${v}${path.extname(file.originalname)}`);
  },
});

const upload = multer({ storage: storage });

router.post("/upload", upload.single("image"), (req, res) => {
  res.json({ path: req.file.filename });
});

router.get("/file/:filename", (req, res) => {
  res.sendFile(path.join(uploadDir, req.params.filename));
});

module.exports = router;
