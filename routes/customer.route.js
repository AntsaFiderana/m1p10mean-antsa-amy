var express = require("express");
const User = require("../models/user.model");
const verifyToken = require("../middleware/authenticator.middleware");
var router = express.Router();

router.get("/:id/favorites", verifyToken, async function (req, res) {
  let query = User.find({ _id: req.user._id });
  if (req.query.full) {
    query
      .populate("favorites.employees", "-workingHours -favorites -password")
      .populate("favorites.services", "-commission");
  }
  const user = await query.exec();
  res.json(user[0].favorites);
});

router.put("/:id/favorites", verifyToken, async (req, res) => {
  const customer = await User.findById(req.user._id);
  const favorite = req.body;

  const index = customer.favorites[favorite.type].findIndex(
    (f) => f._id.toString() == favorite.value
  );

  if (index !== -1) {
    customer.favorites[favorite.type].splice(index, 1);
  } else {
    customer.favorites[favorite.type].push(favorite.value);
  }
  await customer.save();
  res.json({ message: "OK" });
});

module.exports = router;
