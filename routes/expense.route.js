var express = require("express");
const Expense = require("../models/expense.model");
const AppointmentService = require("../service/appointment.service");
var router = express.Router();

router.get("/", async function (req, res) {
  const expenses = await Expense.find().sort({ date: 1 });
  const commissions = await AppointmentService.getPastCommissionPerDay();
  const allExpenses = [...expenses, ...commissions];
  allExpenses.sort((a, b) => a.date - b.date);
  res.json(allExpenses);
});

router.get("/:id", async (req, res) => {
  const expense = await Expense.findById(req.params.id);
  res.json(expense);
});

router.post("/", async (req, res) => {
  const newExpense = new Expense(req.body);
  await newExpense.save();
  res.statusCode = 201;
  res.json({ message: "OK" });
});

router.put("/:id", async (req, res) => {
  await Expense.updateOne({ _id: req.params.id }, req.body);
  res.json({ message: "OK" });
});

module.exports = router;
