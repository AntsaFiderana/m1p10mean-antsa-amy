var express = require("express");
const AppointmentService = require("../service/appointment.service");
var router = express.Router();

router.get("/customers/:id/appointments", async (req, res) => {
  const appointments = await AppointmentService.findAllByCustomer(
    req.params.id
  );
  res.json(appointments);
});

router.get("/employees/:id/appointments", async (req, res) => {
  const appointments = await AppointmentService.findAllByEmployee(
    req.params.id,
    {}
  );
  res.json(appointments);
});

router.get("/appointments/unassigned", async (req, res) => {
  const appointments = await AppointmentService.findAllUnassigned();
  res.json(appointments);
});

router.post("/customers/:id/appointments", async (req, res) => {
  const appointment = req.body;
  appointment.customer = req.params.id;
  const savedAppointment = await AppointmentService.makeAppointment(
    appointment
  );
  res.statusCode = 201;
  res.json(savedAppointment);
});

router.post("/appointments/date-validation", (req, res) => {
  try {
    AppointmentService.validateAppointmentDate(req.body);
    res.json({ message: "OK" });
  } catch (error) {
    res.statusCode = 400;
    res.json(error.paths);
  }
});

module.exports = router;
