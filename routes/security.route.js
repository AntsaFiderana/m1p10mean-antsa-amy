var express = require("express");
const SecurityService = require("../service/security.service");
const { UniqueValidationError } = require("../error/validation.error");
const verifyToken = require("../middleware/authenticator.middleware");
const User = require("../models/user.model");
var router = express.Router();

router.post("/login", async (req, res) => {
  try {
    const userWithToken = await SecurityService.authenticate(req.body);
    res.json(userWithToken);
  } catch (error) {
    if (error instanceof UniqueValidationError) {
      res.statusCode = 401;
      res.json(error.error);
    } else {
      throw error;
    }
  }
});

router.get("/me", verifyToken, async (req, res) => {
  res.json(await User.findById(req.user._id).select("-password"));
});

router.post("/register", async function (req, res) {
  try {
    await SecurityService.register(req.body);
    res.json({ message: "Verification Email Sent" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

router.post("/confirmation", async (req, res) => {
  const isValidConfirmation = await SecurityService.confirmCode(req.body);
  if (!isValidConfirmation) {
    res.statusCode = 400;
    res.json({ message: "Invalid confirmation code" });
  } else {
    res.statusCode = 200;
    res.json({ message: "OK" });
  }
});

module.exports = router;
