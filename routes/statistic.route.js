var express = require("express");
const AppointmentService = require("../service/appointment.service");
const ProfitService = require("../service/profit.service");
var router = express.Router();

router.get("/appointments/day/:year/:month", async (req, res) => {
  const { year, month } = req.params;
  const appointments = await AppointmentService.getAppointmentsPerDay(
    parseInt(year),
    parseInt(month)
  );
  res.json(appointments);
});

router.get("/appointments/month/:year", async (req, res) => {
  const { year } = req.params;
  const appointments = await AppointmentService.getAppointmentsPerMonth(
    parseInt(year)
  );
  res.json(appointments);
});

router.get("/revenues/day/:year/:month", async (req, res) => {
  const { year, month } = req.params;
  const revenues = await AppointmentService.getRevenuesPerDay(
    parseInt(year),
    parseInt(month)
  );
  res.json(revenues);
});

router.get("/revenues/month/:year", async (req, res) => {
  const { year } = req.params;
  const revenues = await AppointmentService.getRevenuesPerMonth(parseInt(year));
  res.json(revenues);
});

router.get("/profits/month/:year", async (req, res) => {
  const { year } = req.params;
  const profits = await ProfitService.getProfitsPerMonth(parseInt(year));
  res.json(profits);
});

router.get("/worktime/week/:year/:month", async (req, res) => {
  const { year, month } = req.params;
  const revenues = await AppointmentService.getEmpWorkTimePerWeek(
    parseInt(year),
    parseInt(month)
  );
  res.json(revenues);
});

router.get("/chart/month/:year", async (req, res) => {
  const { year } = req.params;
  const expenses = await ProfitService.getExpensesArrayPerMonth(parseInt(year));
  const revenues = await AppointmentService.getAllMonthsRevenue(parseInt(year));
  const profits = await ProfitService.getProfitsArrayPerMonth(parseInt(year));

  const data = [
    { series: revenues },
    { series: expenses },
    { series: profits }
  ];

  res.json(data);
});

module.exports = router;
