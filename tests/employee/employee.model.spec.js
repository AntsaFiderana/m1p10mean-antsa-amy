const { expect } = require("chai");
const Employee = require("../../models/employee.model");

describe("Employee model", () => {
  describe.only("findAvailable", () => {
    it("should return all available employees given date is 2024-02-05", async () => {
      const actual = await Employee.findAvailable({
        date: "2024-02-05",
        startTime: new Date("2024-02-05T08:00"),
        endTime: new Date("2024-02-05T10:00"),
      });
      expect(actual.map((emp) => emp.username)).deep.equal([
        "Antsa",
        "John",
        "Richard",
        "Tom",
      ]);
    });

    it("should return all available employees given date is 2024-03-15", async () => {
      const actual = await Employee.findAvailable({
        date: "2024-03-15",
        startTime: new Date("2024-02-05T14:00"),
        endTime: new Date("2024-02-05T16:00"),
      });
      expect(actual.map((emp) => emp.username)).deep.equal([
        "Emma",
        "Alex",
        "Sophie",
      ]);
    });

    it("should return all available employees given date is 2024-01-22 when some employees are prise", async () => {
      const actual = await Employee.findAvailable({
        date: "2024-01-22",
        startTime: new Date("2024-02-05T14:00"),
        endTime: new Date("2024-02-05T16:00"),
      });
      expect(actual.map((emp) => emp.username)).deep.equal([
        "Jean",
        "Emma",
        "Richard",
        "Sophie",
        "David",
      ]);
    });

    it("should return empty array when there are no employee available", async () => {
      const expected = await Employee.findAvailable({
        date: "2024-02-05",
        startTime: new Date("2024-02-05T06:00"),
        endTime: new Date("2024-02-05T10:00"),
      });
      expect(expected).is.empty;
    });
  });

  describe("manageWorkingHours", () => {
    it("should replace the working hours of an employee", async () => {
      const employee = await Employee.findById(employees[1]._id);
      const workingHours = [
        {
          day: 1,
          startTime: "08:00",
          endTime: "15:00",
        },
        {
          day: 4,
          startTime: "08:00",
          endTime: "16:30",
        },
      ];
      await employee.manageWorkingHours(workingHours);

      const updatedEmployee = await Employee.findById(employees[1]._id);

      for (let i = 0; i < workingHours.length; i++) {
        updatedEmployee.workingHours[i].day = workingHours[i].day;
        updatedEmployee.workingHours[i].startTime = workingHours[i].startTime;
        updatedEmployee.workingHours[i].endTime = workingHours[i].endTime;
      }
    });
  });

  describe("findAssigned", () => {
    it("should employees assigned in a given date", async () => {
      const expemployees = await Employee.findAssigned(
        new Date("2024-01-22"),
        new Date("2024-02-22T09:00"),
        new Date("2024-02-22T16:00")
      );

      expect(expemployees.map((e) => e.toString())).deep.equal([
        employees[0]._id.toString(),
        employees[0]._id.toString(),
        employees[0]._id.toString(),
      ]);
    });
  });
});
