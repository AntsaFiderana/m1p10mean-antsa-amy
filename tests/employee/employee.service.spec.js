const { expect } = require("chai");
const { evaluateTaskStatus } = require("../../service/employee.service");
const loader = require("../../fixtures/loader");
const EmployeeService = require("../../service/employee.service");
const employees = require("../../fixtures/employee.data");

const currentDate = new Date("2024-01-22T12:00:00Z");

describe("Employee Service", () => {
  describe("evaluateTaskStatus", () => {
    it("should Termine given end time is before of given date", () => {
      const task = {
        item: {
          startTime: new Date("2024-01-22T08:00Z"),
          endTime: new Date("2024-01-22T10:00Z"),
        },
      };
      evaluateTaskStatus(task, currentDate);
      expect(task.status).equal("Terminée");
    });

    it("should Prochainement given start time is after of given date", () => {
      const task = {
        item: {
          startTime: new Date("2024-01-22T12:30Z"),
          endTime: new Date("2024-01-22T13:00Z"),
        },
      };
      evaluateTaskStatus(task, currentDate);
      expect(task.status).equal("Prochainement");
    });

    it("should En cours given date is between start time and end time", () => {
      const task = {
        item: {
          startTime: new Date("2024-01-22T11:30Z"),
          endTime: new Date("2024-01-22T13:00Z"),
        },
      };
      evaluateTaskStatus(task, currentDate);
      expect(task.status).equal("En cours");
    });
  });

  describe("getDailyTasksByEmployee", () => {
    it("should return daily tasks of employee", async () => {
      const tasks = await EmployeeService.getDailyTasksByEmployee(
        employees[0]._id.toString(),
        new Date("2024-01-22")
      );
      expect(tasks).length(3);
      tasks.forEach((t) => {
        expect(t.item.employee.toString()).equal(employees[0]._id.toString());
      });
    });
  });
});
