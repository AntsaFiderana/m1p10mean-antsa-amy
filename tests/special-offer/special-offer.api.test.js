const { expect } = require("chai");
const specialOffers = require("../../fixtures/special-offer.data");
const { client } = require("../setup/api");
const loader = require("../../fixtures/loader");
const services = require("../../fixtures/service.data");

const specialOffer = {
  service_id: services[0]._id,
  startDate: "2024-02-20T08:00",
  endDate: "2024-03-20T17:00",
  price: {
    amount: 4000,
    currency: "Ar",
  },
};

describe("Special Offer api", () => {
  describe("GET /special-offers", () => {
    it("should return all special offers", async () => {
      await loader();

      const response = await client.get("/special-offers");
      expect(response.statusCode).equal(200);
      expect(response.body.data).length(specialOffers.length);
    });
  });

  describe("POST /special-offers", () => {
    it("should create a new special offer", async () => {
      const response = await client.post("/special-offers").send(specialOffer);
      expect(response.statusCode).equal(201);
    });
  });

  describe("GET /special-offers/current", () => {
    it("should return all current special offers", async () => {
      const response = await client.get("/special-offers/current");
      expect(response.statusCode).equal(200);
    });
  });
});
