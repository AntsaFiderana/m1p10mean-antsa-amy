const { expect } = require("chai");
const customers = require("../../fixtures/customer.data");
const { client, bearer, authorization } = require("../setup/api");

const customer = customers[0];

describe("Security api", () => {
  describe("POST /auth/login", () => {
    it("should return token given valid identifier", async () => {
      const response = await client
        .post("/auth/login")
        .send({ identifier: customer.username, password: customer.password });
      expect(response.statusCode).equal(200);
      expect(response.body).have.nested.property("data.token");
    });

    it("should return error given invalid identifier", async () => {
      const response = await client
        .post("/auth/login")
        .send({ identifier: "invalid", password: customer.password });
      expect(response.statusCode).equal(401);
      expect(response.body).have.nested.property("error.message");
    });
  });

  describe("GET /auth/me", () => {
    it("should return current user of given token", async () => {
      const response = await client
        .get("/auth/me")
        .set(authorization, await bearer(0));
      expect(response.statusCode).equal(200);
    });
  });
});
