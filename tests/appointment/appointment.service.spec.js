const { expect } = require("chai");
const AppointmentService = require("../../service/appointment.service");
const customers = require("../../fixtures/customer.data");
const services = require("../../fixtures/service.data");
const employees = require("../../fixtures/employee.data");
const { sub, add } = require("date-fns");
const ValidationError = require("../../error/validation.error");

const appointment = {
  date: "2024-02-15T12:00:00Z",
  customer: customers[1]._id,
  services: [
    {
      service: services[0],
      employee: employees[0]._id,
      startTime: "2024-02-15T12:00Z",
      endTime: "2024-02-15T12:30Z",
    },
    {
      service: services[1],
      employee: employees[1]._id,
      startTime: "2024-02-15T12:30Z",
      endTime: "2024-02-15T13:30Z",
    },
  ],
};

describe("Appointment service", () => {
  describe("makeAppointment", () => {
    it("should make a new appointment", async () => {
      const savedAppointment = await AppointmentService.makeAppointment(
        appointment
      );
      expect(savedAppointment._id).is.not.null;
    });
  });

  describe.skip("validateAppointmentDate", () => {
    it("should throw error when date is past", () => {
      const pastDate = sub(new Date(), { days: 5 });
      expect(() =>
        AppointmentService.validateAppointmentDate({
          date: pastDate,
          startTime: "10:00",
        })
      )
        .throw(ValidationError)
        .with.nested.property(
          "paths.date[0]",
          "La date de rendez-vous devrait etre future"
        );
    });

    it("should throw error when date is current date", () => {
      expect(() =>
        AppointmentService.validateAppointmentDate({
          date: new Date(),
          startTime: "10:00",
        })
      )
        .throw(ValidationError)
        .with.nested.property(
          "paths.date[0]",
          "La date de rendez-vous devrait etre future"
        );
    });

    it("should throw error when start time is before 07:00", () => {
      const futureDate = add(new Date(), { days: 5 });
      expect(() =>
        AppointmentService.validateAppointmentDate({
          date: futureDate,
          startTime: "05:00",
        })
      ).throw(ValidationError);
    });

    it("should throw error when start time is after 18:00", () => {
      const futureDate = add(new Date(), { days: 5 });
      expect(() =>
        AppointmentService.validateAppointmentDate({
          date: futureDate,
          startTime: "19:00",
        })
      ).throw(ValidationError);
    });

    it("should not throw error when date is future and start time is valid", () => {
      const futureDate = add(new Date(), { days: 5 });
      expect(() =>
        AppointmentService.validateAppointmentDate({
          date: futureDate,
          startTime: "10:00",
        })
      ).not.throw(ValidationError);
    });
  });

  describe("findAllByCustomer", () => {
    it("should return customer appointments", async () => {
      const customer = customers[0]._id;
      const appointmentsHistoric = await AppointmentService.findAllByCustomer(
        customer
      );
      expect(appointmentsHistoric).length(3);
      appointmentsHistoric.forEach((a) => {
        expect(a.customer).deep.equal(customer);
      });
    });
  });

  describe("findAllByEmployee", () => {
    it("should return appointments assigned to given employee", async () => {
      await loader();

      const employee = employees[0]._id;

      const appointments = await AppointmentService.findAllByEmployee(
        employee,
        {}
      );
      expect(appointments).length(4);
      appointments.forEach((a) => {
        a.services.forEach((s) => {
          expect(s.employee.toString()).equal(employee.toString());
        });
      });
    });
  });

  describe("findAllUnassigned", () => {
    it("should return appointments not assigned", async () => {
      const appointments = await AppointmentService.findAllUnassigned();
      expect(appointments).length(2);
      appointments.forEach((a) => {
        a.services.forEach((s) => {
          expect(s.employee).is.undefined;
        });
      });
    });
  });

  describe("findServicesByDate", () => {
    it("should return services of given date when including", async () => {
      const appointments = await AppointmentService.findAllByDate(
        new Date("2024-01-22"),
        new Date("2024-02-22T09:00"),
        new Date("2024-02-22T16:00")
      );

      let expectedServices = getServices(appointments);

      expect(expectedServices.map((s) => s.service.name)).deep.equal([
        services[3].name,
        services[5].name,
        services[3].name,
      ]);
    });

    it("should return services of given date when start including", async () => {
      const appointments = await AppointmentService.findAllByDate(
        new Date("2024-01-22"),
        new Date("2024-02-22T09:00"),
        new Date("2024-02-22T11:20")
      );

      let expectedServices = getServices(appointments);

      expect(expectedServices.map((s) => s.service.name)).deep.equal([
        services[3].name,
        services[5].name,
      ]);
    });

    it("should return services of given date when end including", async () => {
      const appointments = await AppointmentService.findAllByDate(
        new Date("2024-01-22"),
        new Date("2024-02-22T14:20"),
        new Date("2024-02-22T16:20")
      );

      let expectedServices = getServices(appointments);

      expect(expectedServices.map((s) => s.service.name)).deep.equal([
        services[3].name,
      ]);
    });
  });
});

const getServices = (appointments) => {
  let expectedServices = [];
  appointments.forEach(
    (a) => (expectedServices = expectedServices.concat(a.services))
  );
  return expectedServices;
};
