const { expect } = require("chai");
const customers = require("../../fixtures/customer.data");
const { client } = require("../setup/api");
const services = require("../../fixtures/service.data");
const employees = require("../../fixtures/employee.data");
const { add } = require("date-fns");

const appointment = {
  date: "2024-02-15T12:00:00",
  services: [
    {
      service: services[0],
      employee: employees[0]._id,
      startTime: "12:00",
      endTime: "12:30",
    },
    {
      service: services[1],
      employee: employees[1]._id,
      startTime: "12:30",
      endTime: "13:30",
    },
  ],
};

describe("Appointment api", () => {
  describe("POST /customers/:id/appointments", () => {
    it("should make a new appointment for given customer", async () => {
      const response = await client
        .post(`/customers/${customers[0]._id}/appointments`)
        .send(appointment);
      expect(response.statusCode).equal(201);
    });
  });

  // describe("POST /appointments/date-validation", () => {
  //   it("should return 400 code when date or startTime is invalid", async () => {
  //     const response = await client.post(`/appointments/date-validation`).send({
  //       date: "2024-02-01",
  //       startTime: "10:00",
  //     });
  //     expect(response.statusCode).equal(400);
  //   });

  //   it("should return 200 code when date and startTime is valid", async () => {
  //     const futureDate = add(new Date(), { days: 5 });

  //     const response = await client.post(`/appointments/date-validation`).send({
  //       date: futureDate,
  //       startTime: "10:00",
  //     });
  //     expect(response.statusCode).equal(200);
  //   });
  // });

  describe("GET /customers/:id/appointments", () => {
    it("should return appointments historic of given customer", async () => {
      const response = await client.get(
        `/customers/${customers[0]._id}/appointments`
      );
      expect(response.statusCode).equal(200);
    });
  });

  describe("GET /employees/:id/appointments", () => {
    it("should return appointments assigned of given employee", async () => {
      const response = await client.get(
        `/employees/${employees[0]._id}/appointments`
      );
      expect(response.statusCode).equal(200);
    });
  });

  describe("GET /appointments/unassigned", () => {
    it("should return appointments unassigned", async () => {
      const response = await client.get(`/appointments/unassigned`);
      expect(response.statusCode).equal(200);
    });
  });
});
