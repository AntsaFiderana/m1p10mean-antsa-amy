const { expect } = require("chai");
const expenses = require("../../fixtures/expense.data");
const { client } = require("../setup/api");
const loader = require("../../fixtures/loader");

describe("Expense api", () => {
  describe("GET /expenses", () => {
    it("should return all expenses", async () => {
      await loader();

      const response = await client.get("/expenses");
      expect(response.statusCode).equal(200);
      expect(response.body.data).length(expenses.length);
    });
  });

  describe("GET /expenses/:id", () => {
    it("should return expense", async () => {
      const expense = expenses[0];
      const response = await client.get(`/expenses/${expense._id}`);
      expect(response.statusCode).equal(200);
      expect(response.body.data).have.nested.property(
        "price.amount",
        expense.price.amount
      );
      expect(response.body.data).have.nested.property(
        "price.currency",
        expense.price.currency
      );
      expect(response.body.data).to.have.property("name", expense.name);
      expect(response.body.data).to.have.property(
        "date",
        expense.date.toISOString()
      );
    });
  });

  describe("POST /expenses", () => {
    it("should create a new expense", async () => {
      const response = await client.post("/expenses").send({
        name: "Training Workshop",
        price: { amount: 20000, currency: "Ar" },
        date: new Date("2024-02-24"),
      });
      expect(response.statusCode).equal(201);
    });
  });

  describe("PUT /expenses/:id", () => {
    it("should update an existing expense", async () => {
      const expense = expenses[0];

      const response = await client.put(`/expenses/${expense._id}`).send({
        name: expense.name,
        price: { amount: 25000, currency: expense.price.currency },
        date: expense.date,
      });
      expect(response.statusCode).equal(200);
    });
  });
});
