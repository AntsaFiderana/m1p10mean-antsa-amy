const { expect } = require("chai");
const Service = require("../../models/service.model");

const service = {
  name: "Service under test",
  duration: 30,
  price: {
    amount: 10000,
    currency: "Ar",
  },
  commission: 20,
};

describe("Service Model", () => {
  describe("commissionAmount", () => {
    it("should return 2000 given price is 10000 and commission is 20", async () => {
      const doc = await Service.create(service);
      expect(doc.commissionAmount).equal(2000);
    });
  });
});
