const { expect } = require("chai");
const services = require("../../fixtures/service.data");
const { client } = require("../setup/api");
const loader = require("../../fixtures/loader");

describe("Service api", () => {
  describe("POST /services", () => {
    it("should create a new service", async () => {
      const response = await client.post("/services").send({
        price: {
          amount: 10000,
          currency: "Ar",
        },
        name: "Maquillage",
        duration: 25,
        commission: 20,
      });
      expect(response.statusCode).equal(201);
    });
  });

  describe("PUT /services/:id", () => {
    it("should update an existing service", async () => {
      const service = services[0];

      const response = await client.put(`/services/${service._id}`).send({
          price: {
            amount: 20000,
            currency: service.price.currency,
          },
          name: service.name,
          duration: service.duration,
          commission: service.commission,
        });
      expect(response.statusCode).equal(200);
    });
  });

  describe("GET /services/:id", () => {
    it("should return service", async () => {
      const service = services[1];
      const response = await client.get(`/services/${service._id}`);
      expect(response.statusCode).equal(200);
      expect(response.body.data).have.nested.property("price.amount", service.price.amount);
      expect(response.body.data).have.nested.property("price.currency", service.price.currency);
      expect(response.body.data).to.have.property("name", service.name);
      expect(response.body.data).to.have.property("duration", service.duration);
      expect(response.body.data).to.have.property("commission", service.commission);
    })
  });

  describe("GET /services", () => {
    it("should return all services", async () => {
      await loader();

      const response = await client.get("/services");
      expect(response.statusCode).equal(200);
      expect(response.body.data).length(services.length);
    })
  });

});
