const { expect } = require("chai");
const AppointmentService = require("../../service/appointment.service");
const ProfitService = require("../../service/profit.service");
const expenses = require("../../fixtures/expense.data");
const { format } = require("date-fns");

const year = 2024;
const month = 1;

describe("Statistic service", () => {
  describe("getAppointmentsPerDay", () => {
    it("should return number of appointments per day by year & month", async () => {
      const mockAppointmentsPerDay = [
        { _id: "2024-01-20", count: 1 },
        { _id: "2024-01-22", count: 2 },
      ];
      const appointmentsPerDay = await AppointmentService.getAppointmentsPerDay(
        year,
        month
      );
      expect(appointmentsPerDay).to.deep.equal(mockAppointmentsPerDay);
    });
  });

  describe("getAppointmentsPerMonth", () => {
    it("should return number of appointments per month by year", async () => {
      const mockAppointmentsPerMonth = [
        { _id: "2024-01", count: 3 },
        { _id: "2024-02", count: 2 },
        { _id: "2024-03", count: 1 },
      ];
      const appointmentsPerMonth =
        await AppointmentService.getAppointmentsPerMonth(year);
      expect(appointmentsPerMonth).to.deep.equal(mockAppointmentsPerMonth);
    });
  });

  describe("getRevenuesPerDay", () => {
    it("should return total revenues per day by year & month", async () => {
      const mockRevenuesPerDay = [
        { _id: "2024-01-20", count: 7000 },
        { _id: "2024-01-22", count: 18400 },
      ];
      const revenuesPerDay = await AppointmentService.getRevenuesPerDay(
        year,
        month
      );
      expect(revenuesPerDay).to.deep.equal(mockRevenuesPerDay);
    });
  });

  describe("getRevenuesPerMonth", () => {
    it("should return total revenues per month by year", async () => {
      const mockRevenuesPerMonth = [
        { _id: "2024-01", count: 25400 },
        { _id: "2024-02", count: 14000 },
        { _id: "2024-03", count: 13400 },
      ];
      const revenuesPerMonth = await AppointmentService.getRevenuesPerMonth(
        year
      );
      expect(revenuesPerMonth).to.deep.equal(mockRevenuesPerMonth);
    });
  });

  describe("getExpensesPerMonth", () => {
    it("should return total expenses per month by year", async () => {
      const mockExpensesPerMonth = [
        {
          _id: format(expenses[0].date, "yyyy-MM"),
          count: expenses
            .slice(0, 5)
            .reduce((total, item) => total - item.price.amount, 0),
        },
        {
          _id: format(expenses[5].date, "yyyy-MM"),
          count: expenses
            .slice(5, 10)
            .reduce((total, item) => total - item.price.amount, 0),
        },
      ];
      const expensesPerMonth = await ProfitService.getExpensesPerMonth(year);
      expect(expensesPerMonth).to.deep.equal(mockExpensesPerMonth);
    });
  });

  describe("getProfitsPerMonth", () => {
    it("should return total profits per month by year", async () => {
      const mockProfitsPerMonth = [
        { _id: "2024-01", count: 20400 },
        { _id: "2024-02", count: 9000 },
        { _id: "2024-03", count: 13400 },
      ];
      const profitsPerMonth = await ProfitService.getProfitsPerMonth(year);
      expect(profitsPerMonth).to.deep.equal(mockProfitsPerMonth);
    });
  });
});
