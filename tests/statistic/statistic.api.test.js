const { expect } = require("chai");
const services = require("../../fixtures/service.data");
const { client } = require("../setup/api");
const loader = require("../../fixtures/loader");

describe("Statistic api", () => {
  describe("GET /statistics/appointments/day/:year/:month", () => {
    const year = 2024;
    const month = 1;
    it("should return number of appointments per day by year & month", async () => {
      const response = await client.get(
        `/statistics/appointments/day/${year}/${month}`
      );
      expect(response.statusCode).equal(200);
    });
  });

  describe("GET /statistics/appointments/month/:year", () => {
    const year = 2024;
    it("should return number of appointments per month by year", async () => {
      const response = await client.get(
        `/statistics/appointments/month/${year}`
      );
      expect(response.statusCode).equal(200);
    });
  });

  describe("GET /statistics/revenues/day/:year/:month", () => {
    const year = 2024;
    const month = 1;
    it("should return total revenues per day by year & month", async () => {
      const response = await client.get(
        `/statistics/revenues/day/${year}/${month}`
      );
      expect(response.statusCode).equal(200);
    });
  });

  describe("GET /statistics/revenues/month/:year", () => {
    const year = 2024;
    it("should return total revenues per month by year", async () => {
      const response = await client.get(`/statistics/revenues/month/${year}`);
      expect(response.statusCode).equal(200);
    });
  });

  describe("GET /statistics/profits/month/:year", () => {
    const year = 2024;
    it("should return total profits per month by year", async () => {
      const response = await client.get(`/statistics/profits/month/${year}`);
      expect(response.statusCode).equal(200);
    });
  });
});
