const { default: mongoose } = require("mongoose");
const http = require("http");
const app = require("../../app");
const loader = require("../../fixtures/loader");
const SecurityService = require("../../service/security.service");
const customers = require("../../fixtures/customer.data");

const server = http.createServer(app);
server.listen(3002);
const client = require("supertest")(server);

const bearer = async (userId) => {
  const token = await SecurityService.authenticate({
    identifier: customers[userId].username,
    password: customers[userId].password,
  });
  return `Bearer ${token}`;
};

const authorization = "Authorization";

module.exports = {
  mochaHooks: {
    async beforeAll() {
      await loader();
    },
    afterAll() {
      server.close();
      mongoose.connection.close();
    },
  },
  client,
  bearer,
  authorization,
};
