const { default: mongoose } = require("mongoose");
const loader = require("../../fixtures/loader");

exports.mochaHooks = {
  async beforeAll() {
    await loader();
  },
  afterAll() {
    mongoose.connection.close();
  },
};
