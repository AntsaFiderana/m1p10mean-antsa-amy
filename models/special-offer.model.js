const { Schema, default: mongoose } = require("mongoose");
const Service = require("./service.model");

const schema = new Schema({
  service: Service.schema,
  startDate: Date,
  endDate: Date,
  price: {
    amount: Number,
    currency: String,
  },
});

const SpecialOffer = mongoose.model("SpecialOffer", schema);

module.exports = SpecialOffer;
