const { isWithinInterval } = require("date-fns");
const { Schema, default: mongoose } = require("mongoose");

const schema = new Schema(
  {
    name: String,
    price: {
      amount: Number,
      currency: String,
    },
    duration: Number,
    commission: Number,
    photo: String,
    promotion: { type: Schema.Types.ObjectId, ref: "SpecialOffer" },
    description: String,
    isDeleted: { type: Boolean, default: false },
  },
  {
    virtuals: {
      commissionAmount: {
        get() {
          return (this.price.amount * this.commission) / 100;
        },
      },
      promotionPrice: {
        get() {
          if (this.promotion) {
            if (
              isWithinInterval(new Date(), {
                start: this.promotion.startDate,
                end: this.promotion.endDate,
              })
            ) {
              return this.promotion.price.amount;
            }
          }
        },
      },
    },
    toJSON: { virtuals: true },
  }
);

const Service = mongoose.model("Service", schema);

module.exports = Service;
