const { default: mongoose, Schema } = require("mongoose");

const User = mongoose.model(
  "User",
  new mongoose.Schema(
    {
      username: { type: String, unique: true },
      email: { type: String },
      password: String,
      firstName: String,
      lastName: String,
      adresse: String,
      roles: [String],
      confirmationCode: String,
      isConfirmed: { type: Boolean, default: false },
      photo: String,
      isDeleted: { type: Boolean, default: false },
      favorites: {
        employees: [{ type: Schema.Types.ObjectId, ref: "Employee" }],
        services: [{ type: Schema.Types.ObjectId, ref: "Service" }],
      },
      preferences: {
        notification: { type: Boolean, default: true },
        rappel: { type: Boolean, default: true },
      },
    },
    {
      virtuals: {
        name: {
          get() {
            return `${this.firstName}  ${this.lastName}`;
          },
        },
      },
      toJSON: { virtuals: true },
    }
  )
);

User.findByIdentifier = function ({ identifier, password }) {
  return User.findOne({
    $or: [{ username: identifier }, { email: identifier }],
    password: password,
  }).select("-password");
};

module.exports = User;
