const { default: mongoose, Schema } = require("mongoose");
const Service = require("./service.model");

const Appointment = mongoose.model(
  "Appointment",
  new Schema(
    {
      date: Date,
      customer: { type: Schema.Types.ObjectId, ref: "User" },
      services: [
        {
          service: Service.schema,
          employee: { type: Schema.Types.ObjectId, ref: "Employee" },
          startTime: Date,
          endTime: Date,
          commissionAmount: Number,
        },
      ],
      reminderSent: { type: Boolean, default: false },
    },
    {
      virtuals: {
        totalAmount: {
          get() {
            return this.services?.reduce(
              (sum, item) => sum + item.service.price.amount,
              0
            );
          },
        },
        totalDuration: {
          get() {
            return this.services?.reduce(
              (sum, item) => sum + item.service.duration,
              0
            );
          },
        },
      },
      toJSON: { virtuals: true },
    }
  )
);

module.exports = Appointment;
