const { Schema } = require("mongoose");
const User = require("./user.model");
const {
  getDayOfWeek,
  replaceDate,
  getReferenceDate,
} = require("../utils/date.utils");
const AppointmentService = require("../service/appointment.service");

const Employee = User.discriminator(
  "Employee",
  new Schema(
    {
      workingHours: [
        {
          day: { type: Number, required: true },
          startTime: { type: Date, required: true },
          endTime: { type: Date, required: true },
        },
      ],
    },
    {
      discriminatorKey: "kind",
    }
  )
);

Employee.findAvailable = async function ({ date, startTime, endTime }) {
  const prisesEmployee = await Employee.findAssigned(date, startTime, endTime);
  replaceDate(startTime, getReferenceDate());
  replaceDate(endTime, getReferenceDate());
  return await Employee.find({
    _id: {
      $nin: prisesEmployee,
    },
    isDeleted: false,
    workingHours: {
      $elemMatch: {
        day: getDayOfWeek(date),
        startTime: { $lte: startTime },
        endTime: { $gte: endTime },
      },
    },
  }).select("-workingHours -roles");
};

Employee.findAssigned = async function (date, startTime, endTime) {
  const appointments = await AppointmentService.findAllByDate(
    date,
    startTime,
    endTime
  );
  let employeesId = [];
  appointments.forEach((a) => {
    employeesId = employeesId.concat(a.services.map((s) => s.employee));
  });
  return employeesId;
};

module.exports = Employee;
