const { Schema, default: mongoose } = require("mongoose");

const schema = new Schema(
  {
    name: String,
    price: {
      amount: Number,
      currency: String,
    },
    date: Date,
  },
);

const Expense = mongoose.model("Expense", schema);

module.exports = Expense;
