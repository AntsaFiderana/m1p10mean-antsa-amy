const { default: mongoose } = require("mongoose");

mongoose
  .connect(process.env.MONGO_URL)
  .then(() => console.log("Database connected"))
  .catch((error) => {
    throw Error("Database connection error " + error);
  });
