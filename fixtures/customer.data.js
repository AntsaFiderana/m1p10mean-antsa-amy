const { fixtures } = require("antsa-mongoose-fixture");
const services = require("./service.data");
const employees = require("./employee.data");

const customers = [
  {
    username: "Yamada",
    email: "aandrianiaina1@gmail.com",
    password: "0",
    firstName: "Yamada",
    lastName: "Anna",
    roles: ["CUSTOMER"],
    favorites: {
      services: [services[0]._id, services[2]._id, services[4]._id],
      employees: [employees[0]._id, employees[1]._id],
    },
  },
  {
    username: "Rachid",
    email: "antsafiderana333@gmail.com",
    password: "user",
    firstName: "Rachid",
    lastName: "Bill",
    roles: ["CUSTOMER"],
    favorites: {
      services: [services[2]._id, services[3]._id],
      employees: [employees[0]._id],
    },
  },
  {
    username: "Clemont",
    email: "clemont@gmail.com",
    password: "1",
    firstName: "Clemont",
    lastName: "Davis",
    roles: ["CUSTOMER"],
    favorites: {
      services: [services[6]._id, services[3]._id, services[5]._id],
      employees: [employees[2]._id, employees[3]._id],
    },
  },
  {
    username: "Pascaline",
    email: "pascaline@gmail.com",
    password: "1",
    firstName: "Pascaline",
    lastName: "Laris",
    roles: ["CUSTOMER"],
    favorites: {
      services: [services[0]._id, services[3]._id, services[5]._id],
      employees: [employees[0]._id, employees[3]._id],
    },
  },
  {
    username: "Linda",
    email: "linda@gmail.com",
    password: "1",
    firstName: "Linda",
    lastName: "Davalor",
    roles: ["CUSTOMER"],
    favorites: {
      services: [services[2]._id, services[3]._id],
      employees: [employees[1]._id, employees[3]._id],
    },
  },
  {
    username: "Ricardo",
    email: "ricardo@gmail.com",
    password: "1",
    firstName: "Ricardo",
    lastName: "Perlo",
    roles: ["CUSTOMER"],
    favorites: {
      services: [services[3]._id, services[2]._id],
      employees: [employees[1]._id, employees[2]._id],
    },
  },
  {
    username: "AmyAndrianiaina",
    email: "aandrianiaina1@gmail.com",
    password: "2",
    firstName: "Amy",
    lastName: "Andrianiaiina",
    roles: ["CUSTOMER"],
    favorites: {
      services: [services[1]._id, services[2]._id, services[4]._id],
      employees: [employees[0]._id, employees[2]._id],
    },
  },
  {
    username: "Admin",
    email: "admin@gmail.com",
    password: "2",
    firstName: "Admin",
    lastName: "Admin",
    roles: ["ADMIN"],
  },
];

fixtures.addId(customers);

module.exports = customers;
