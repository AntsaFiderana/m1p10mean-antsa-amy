const { fixtures } = require("antsa-mongoose-fixture");

const services = [
  {
    name: "Soins du visage",
    price: {
      amount: 5000,
      currency: "Ar",
    },
    duration: 30,
    commission: 20,
    photo: "visage.jpg",
    description: "Une expérience sensorielle revitalisante.",
  },
  {
    name: "Épilation à la cire",
    price: {
      amount: 10000,
      currency: "Ar",
    },
    duration: 60,
    commission: 40,
    photo: "epilation.jpg",
    description: "Une épilation douce et précise pour une peau lisse .",
  },
  {
    name: "Massage relaxant",
    price: {
      amount: 6000,
      currency: "Ar",
    },
    duration: 90,
    commission: 50,
    photo: "massage.jpg",
    description: "Un voyage apaisant pour le corps et l'esprit.",
  },
  {
    name: "Manucure et pédicure",
    price: {
      amount: 8000,
      currency: "Ar",
    },
    duration: 60,
    commission: 20,
    photo: "ongle.jpg",
    description: "Des soins délicats pour sublimer vos mains et pieds.",
  },
  {
    name: "Coupe de cheveux",
    price: {
      amount: 20000,
      currency: "Ar",
    },
    duration: 40,
    commission: 25,
    photo: "coiffure.jpg",
    description: "Une coupe de cheveux personnalisée, pour style unique .",
  },
  {
    name: "Extensions de cils",
    price: {
      amount: 7000,
      currency: "Ar",
    },
    duration: 30,
    commission: 20,
    photo: "extension.jpg",
    description: "Des cils sublimés et naturellement glamour.",
  },
  {
    name: "Maquillage professionnel",
    price: {
      amount: 10000,
      currency: "Ar",
    },
    duration: 40,
    photo: "maquillage.jpg",
    commission: 40,
    description: "Une transformation artistique qui sublime votre beauté.",
  },
  {
    name: "Bronzage sans UV",
    price: {
      amount: 15000,
      currency: "Ar",
    },
    duration: 50,
    commission: 30,
    photo: "bronze.jpg",
    description: "Un bronzage sans UV, éclatant et naturel.",
  },
  {
    name: "Tressage",
    price: {
      amount: 6000,
      currency: "Ar",
    },
    duration: 50,
    commission: 25,
    photo: "tresse.jpg",
    description: "Un tressage expert et créatif avec des styles uniques .",
  },
];

fixtures.addId(services);

module.exports = services;
