const { db } = require("../models/service.model");
const loader = require("./loader");

loader().then(() => {
  console.log("Fixtures loaded");
  db.close();
});
