const { fixtures } = require("antsa-mongoose-fixture");
const services = require("./service.data");
const Service = require("../models/service.model");
const employees = require("./employee.data");
const Employee = require("../models/employee.model");
const customers = require("./customer.data");
const User = require("../models/user.model");
const appointments = require("./appointment.data");
const Appointment = require("../models/appointment.model");
const expenses = require("./expense.data");
const Expense = require("../models/expense.model");
const specialOffers = require("./special-offer.data");
const SpecialOffer = require("../models/special-offer.model");

const envFile = process.env.NODE_ENV === "production" ? ".env.prod" : ".env";
require("dotenv").config({ path: envFile });
require("../config/db");

const loader = async () => {
  await fixtures.load(
    [services, Service],
    [employees, Employee],
    [customers, User],
    [appointments, Appointment],
    [expenses, Expense],
    [specialOffers, SpecialOffer]
  );
};

module.exports = loader;
