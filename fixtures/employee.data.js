const { fixtures } = require("antsa-mongoose-fixture");
const { getReferenceDate } = require("../utils/date.utils");

const employees = [
  {
    username: "Antsa",
    password: "emp",
    email: "antsa@gmail.com",
    firstName: "Antsa",
    lastName: "Fiderana",
    roles: ["EMP"],
    workingHours: [
      {
        day: 1,
        startTime: getReferenceDate() + "T08:00",
        endTime: getReferenceDate() + "T17:00",
      },
      {
        day: 2,
        startTime: getReferenceDate() + "T09:00",
        endTime: getReferenceDate() + "T17:00",
      },
      {
        day: 3,
        startTime: getReferenceDate() + "T10:00",
        endTime: getReferenceDate() + "T18:00",
      },

      {
        day: 4,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 5,
        startTime: getReferenceDate() + "T09:00",
        endTime: getReferenceDate() + "T13:00",
      },
    ],
  },
  {
    username: "John",
    password: "emp",
    email: "john@gmail.com",
    firstName: "John",
    lastName: "Doe",
    roles: ["EMP"],
    photo: "emp2.jpg",
    workingHours: [
      {
        day: 1,
        startTime: getReferenceDate() + "T08:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 2,
        startTime: getReferenceDate() + "T09:00",
        endTime: getReferenceDate() + "T17:00",
      },
      {
        day: 3,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 4,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },

      {
        day: 5,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
    ],
  },
  {
    username: "Jean",
    password: "emp",
    email: "jean@gmail.com",
    firstName: "Jean",
    lastName: "Arc",
    roles: ["EMP"],
    photo: "emp3.jpg",
    workingHours: [
      {
        day: 1,
        startTime: getReferenceDate() + "T10:00",
        endTime: getReferenceDate() + "T18:00",
      },

      {
        day: 2,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 3,
        startTime: getReferenceDate() + "T08:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 4,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 5,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T13:00",
      },
    ],
  },
  {
    username: "Emma",
    password: "emp",
    email: "emma@gmail.com",
    firstName: "Emma",
    lastName: "Scarlet",
    roles: ["EMP"],
    photo: "emp4.jpg",
    workingHours: [
      {
        day: 1,
        startTime: getReferenceDate() + "T08:00",
        endTime: getReferenceDate() + "T18:00",
      },
      {
        day: 2,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 3,
        startTime: getReferenceDate() + "T08:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 4,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 5,
        startTime: getReferenceDate() + "T08:00",
        endTime: getReferenceDate() + "T18:00",
      },
    ],
  },
  {
    username: "Richard",
    password: "emp",
    email: "richard@gmail.com",
    firstName: "Richard",
    lastName: "Markson",
    roles: ["EMP"],
    photo: "emp5.jpg",
    workingHours: [
      {
        day: 1,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 2,
        startTime: getReferenceDate() + "T09:00",
        endTime: getReferenceDate() + "T17:00",
      },
      {
        day: 3,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 4,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 5,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T13:00",
      },
    ],
  },

  {
    username: "Alex",
    password: "emp",
    email: "alex@gmail.com",
    firstName: "Alex",
    lastName: "Foley",
    roles: ["EMP"],
    photo: "emp6.jpeg",
    workingHours: [
      {
        day: 1,
        startTime: getReferenceDate() + "T08:00",
        endTime: getReferenceDate() + "T17:00",
      },
      {
        day: 2,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 3,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 4,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 5,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T17:00",
      },
    ],
  },
  {
    username: "Sophie",
    password: "emp",
    email: "sohpie@gmail.com",
    firstName: "Sophie",
    lastName: "Christiane",
    roles: ["EMP"],
    photo: "emp7.jpeg",
    workingHours: [
      {
        day: 1,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 2,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T17:00",
      },
      {
        day: 3,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 4,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T14:00",
      },
      {
        day: 5,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
    ],
  },
  {
    username: "Tom",
    password: "emp",
    email: "tom@gmail.com",
    firstName: "Tom",
    lastName: "Sawer",
    roles: ["EMP"],
    photo: "emp8.jpeg",
    workingHours: [
      {
        day: 1,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 2,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 3,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T17:00",
      },
      {
        day: 4,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T14:00",
      },
      {
        day: 5,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
    ],
  },
  {
    username: "Linda",
    password: "emp",
    email: "linda@gmail.com",
    firstName: "Linda",
    lastName: "Vegas",
    roles: ["EMP"],
    workingHours: [
      {
        day: 1,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 2,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 3,
        startTime: getReferenceDate() + "T08:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 4,
        startTime: getReferenceDate() + "T08:00",
        endTime: getReferenceDate() + "T17:00",
      },
      {
        day: 5,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
    ],
  },
  {
    username: "David",
    password: "emp",
    email: "david@gmail.com",
    firstName: "David",
    lastName: "Jonhs",
    roles: ["EMP"],
    workingHours: [
      {
        day: 1,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T16:00",
      },
      {
        day: 2,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 3,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T17:00",
      },
      {
        day: 4,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
      {
        day: 5,
        startTime: getReferenceDate() + "T07:00",
        endTime: getReferenceDate() + "T15:00",
      },
    ],
  },
];

fixtures.addId(employees);

module.exports = employees;
