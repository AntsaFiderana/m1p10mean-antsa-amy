const { fixtures } = require("antsa-mongoose-fixture");

const expenses = [
  {
    name: "Office Supplies",
    price: { amount: 1000, currency: "Ar" },
    date: new Date("2024-01-05"),
  },
  {
    name: "Travel Expenses",
    price: { amount: 1000, currency: "Ar" },
    date: new Date("2024-01-10"),
  },
  {
    name: "Dinner Meeting",
    price: { amount: 1000, currency: "Ar" },
    date: new Date("2024-01-15"),
  },
  {
    name: "Software Subscription",
    price: { amount: 1000, currency: "Ar" },
    date: new Date("2024-01-20"),
  },
  {
    name: "Client Gift",
    price: { amount: 1000, currency: "Ar" },
    date: new Date("2024-01-25"),
  },
  {
    name: "Office Rent",
    price: { amount: 1000, currency: "Ar" },
    date: new Date("2024-02-05"),
  },
  {
    name: "Marketing Campaign",
    price: { amount: 1000, currency: "Ar" },
    date: new Date("2024-02-10"),
  },
  {
    name: "Team Building Event",
    price: { amount: 1000, currency: "Ar" },
    date: new Date("2024-02-15"),
  },
  {
    name: "Website Hosting",
    price: { amount: 1000, currency: "Ar" },
    date: new Date("2024-02-20"),
  },
  {
    name: "Training Workshop",
    price: { amount: 1000, currency: "Ar" },
    date: new Date("2024-02-25"),
  },
];

fixtures.addId(expenses);

module.exports = expenses;
