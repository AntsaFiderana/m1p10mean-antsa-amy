const { fixtures } = require("antsa-mongoose-fixture");
const services = require("./service.data");

const specialOffers = [
  {
    service: services[3],
    startDate: "2024-02-27T08:00",
    endDate: "2024-03-27T17:00",
    price: {
      amount: 5000,
      currency: "Ar",
    },
  },
];

fixtures.addId(specialOffers);

module.exports = specialOffers;
