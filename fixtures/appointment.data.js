const { fixtures } = require("antsa-mongoose-fixture");
const customers = require("./customer.data");
const employees = require("./employee.data");
const services = require("./service.data");

const appointments = [
  {
    date: "2024-01-22",
    customer: customers[0]._id,
    services: [
      {
        service: services[3],
        employee: employees[0]._id,
        startTime: "2024-01-22T10:00",
        endTime: "2024-01-22T11:00",
      },
      {
        service: services[5],
        employee: employees[0]._id,
        startTime: "2024-01-22T11:00",
        endTime: "2024-01-22T11:30",
      },
    ],
  },
  {
    date: "2024-01-22",
    customer: customers[1]._id,
    services: [
      {
        service: services[3],
        employee: employees[0]._id,
        startTime: "2024-01-22T14:00",
        endTime: "2024-01-22T15:00",
      },
    ],
  },
  {
    date: "2024-01-22",
    customer: customers[1]._id,
    services: [
      {
        service: services[3],
        employee: employees[5]._id,
        startTime: "2024-01-22T14:00",
        endTime: "2024-01-22T15:00",
      },
    ],
  },
  {
    date: "2024-02-14",
    customer: customers[0]._id,
    services: [
      {
        service: services[0],
        employee: employees[1]._id,
        startTime: "2024-02-14T10:00",
        endTime: "2024-02-14T10:30",
      },
      {
        service: services[2],
        employee: employees[1]._id,
        startTime: "2024-02-14T10:30",
        endTime: "2024-02-14T12:00",
      },
    ],
  },
  {
    date: "2024-03-14",
    customer: customers[1]._id,
    services: [
      {
        service: services[0],
        employee: employees[0]._id,
        startTime: "2024-03-14T10:00",
        endTime: "2024-03-14T10:30",
      },
      {
        service: services[2],
        employee: employees[1]._id,
        startTime: "2024-03-14T10:30",
        endTime: "2024-03-14T12:00",
      },
      {
        service: services[3],
        startTime: "2024-03-14T12:00",
        endTime: "2024-03-14T13:00",
      },
    ],
  },
  {
    date: "2024-02-29",
    customer: customers[0]._id,
    services: [
      {
        service: services[0],
        employee: employees[0]._id,
        startTime: "2024-02-29T10:00",
        endTime: "2024-02-29T10:30",
      },
      {
        service: services[2],
        employee: employees[0]._id,
        startTime: "2024-02-29T10:30",
        endTime: "2024-02-29T12:00",
      },
    ],
  },
  {
    date: "2024-01-20",
    customer: customers[1]._id,
    services: [
      {
        service: services[0],
        employee: employees[4]._id,
        startTime: "2024-01-20T10:00",
        endTime: "2024-01-20T10:30",
      },
      {
        service: services[2],
        startTime: "2024-01-20T10:30",
        endTime: "2024-01-20T12:00",
      },
    ],
  },
  {
    date: "2024-02-27T00:00",
    customer: customers[2]._id,
    services: [
      {
        service: services[3],
        employee: employees[0]._id,
        startTime: "2024-02-27T00:00",
        endTime: "2024-02-27T01:00",
      },
      {
        service: services[5],
        employee: employees[0]._id,
        startTime: "2024-02-27T01:00",
        endTime: "2024-02-27T02:00",
      },
    ],
  },
];

fixtures.addId(appointments);

module.exports = appointments;
