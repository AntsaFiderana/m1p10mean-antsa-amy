const fs = require("fs");
const ejs = require("ejs");
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(
  "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"
);
const { readableDate, readableTime } = require("../utils/date.utils");
const htmlFilePath = "templates/email/";

const sendEmail = async (emailTo, subject, templateName, data) => {
  try {
    const htmlContent = await htmlTemplate(templateName, data);

    const msg = {
      to: emailTo,
      from: process.env.SENDER_EMAIL,
      subject: subject,
      html: htmlContent,
    };

    await sgMail.send(msg);
  } catch (error) {
    console.error("Error sending email:", error);
    //throw new Error("Erreur lors de l'envoie des emails");
  }
};

const htmlTemplate = async (templateName, data) => {
  const htmlFile = htmlFilePath + templateName + ".html";
  const htmlContent = await fs.readFileSync(htmlFile, "utf8");
  data.readableDate = readableDate;
  data.readableTime = readableTime;
  const htmlContentWithValues = ejs.render(htmlContent, data);
  return htmlContentWithValues;
};

const sendConfirmationEmail = async (email, confirmationCode) => {
  const msg = {
    to: email,
    from: process.env.SENDER_EMAIL,
    subject: "Beauty Sallon - Code de confirmation pour votre inscription",
    text: `Merci de vous être inscrit(e) sur Beauty Salon ! 
    Pour finaliser votre inscription, veuillez entrer le code de confirmation ci-dessous :

    Code de Confirmation : ${confirmationCode}
    
    Cordialement,

    L'équipe de Beauty Sallon
    `,
  };
  try {
    await sgMail.send(msg);
  } catch (error) {
    console.log(error);
    throw new Error("Failed to send confirmation email");
  }
};

const EmailService = {
  sendConfirmationEmail,
  htmlTemplate,
  sendEmail,
};

module.exports = EmailService;
