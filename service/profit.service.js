const Expense = require("../models/expense.model");
const AppointmentService = require("./appointment.service");

const getExpensesPerMonth = async (year) => {
  const expensesPerMonth = await Expense.aggregate([
    {
      $match: {
        date: {
          $gte: new Date(year, 0, 1),
          $lte: new Date(year, 11, 31, 23, 59, 59, 999),
        },
      },
    },
    {
      $group: {
        _id: { $dateToString: { format: "%Y-%m", date: "$date" } },
        count: { $sum: { $multiply: ["$price.amount", -1] } },
      },
    },
    {
      $sort: {
        _id: 1,
      },
    },
  ]);

  return expensesPerMonth;
};

const getProfitsPerMonth = async (year) => {
  const revenues = await AppointmentService.getRevenuesPerMonth(year);
  const expenses = await getExpensesPerMonth(year);
  const commissions = await AppointmentService.getCommissionPerMonth(year);

  const profitData = [];

  profitData.push(...revenues);
  profitData.push(...expenses);
  profitData.push(...commissions);

  const profitPerMonth = profitData.reduce((result, item) => {
    const { _id, count } = item;
    const existingMonth = result.find((month) => month._id === _id);

    if (existingMonth) {
      existingMonth.count += count;
    } else {
      result.push({ _id, count });
    }

    return result;
  }, []);

  return profitPerMonth;
};

const getProfitsArrayPerMonth = async (year) => {
  const revenues = await AppointmentService.getRevenuesPerMonth(year);
  const expenses = await getExpensesPerMonth(year);
  const commissions = await AppointmentService.getCommissionPerMonth(year);

  const months = Array.from({ length: 12 }, (_, index) => new Date(year, index, 1));
  const monthStrings = months.map(date => date.toISOString().slice(0, 7));

  const profitsArray = monthStrings.map(month => {
    const revenue = revenues.find(item => item._id === month)?.count || 0;
    const expense = expenses.find(item => item._id === month)?.count || 0;
    const commission = commissions.find(item => item._id === month)?.count || 0;
    return (revenue + expense + commission) / 1000;
  });

  return profitsArray;
};

const getExpensesArrayPerMonth = async (year) => {
  const expenses = await getExpensesPerMonth(year);
  const commissions = await AppointmentService.getCommissionPerMonth(year);

  const months = Array.from({ length: 12 }, (_, index) => new Date(year, index, 1));
  const monthStrings = months.map(date => date.toISOString().slice(0, 7));

  const profitsArray = monthStrings.map(month => {
    const expense = expenses.find(item => item._id === month)?.count || 0;
    const commission = commissions.find(item => item._id === month)?.count || 0;
    return (- expense - commission) / 1000;
  });

  return profitsArray;
};

const ProfitService = {
  getExpensesPerMonth,
  getProfitsPerMonth,
  getProfitsArrayPerMonth,
  getExpensesArrayPerMonth,
};

module.exports = ProfitService;
