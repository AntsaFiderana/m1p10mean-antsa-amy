const ValidationError = require("../error/validation.error");
const Appointment = require("../models/appointment.model");
const { isBefore, isToday, parse, isWithinInterval } = require("date-fns");
const {
  parseTime,
  replaceDate,
  getReferenceDate,
} = require("../utils/date.utils");
const User = require("../models/user.model");

const findAllByCustomer = async (customerId) => {
  return await Appointment.find({ customer: customerId })
    .sort({ date: -1 })
    .populate("services.employee", "firstName lastName name")
    .exec();
};

const makeAppointment = async (appointment) => {
  appointment.services.forEach((i) => {
    if (i.service.promotionPrice) {
      i.service.price.amount = i.service.promotionPrice;
    }
  });

  const appointmentDocument = new Appointment(appointment);
  return await appointmentDocument.save();
};

const findAllByEmployee = async (employeeId, query) => {
  query["services.employee"] = employeeId;

  const appointments = await Appointment.find(query)
    .sort({ date: -1 })
    .populate("customer", "firstName lastName name")
    .exec();
  onlyMathedServices(appointments, employeeId);
  return appointments;
};

const findAllUnassigned = async () => {
  const appointments = await Appointment.find({
    "services.employee": null,
  })
    .sort({ date: -1 })
    .populate("customer", "firstName lastName name")
    .exec();
  appointments.forEach((a) => {
    a.services = a.services.filter((s) => s.employee == undefined);
  });
  return appointments;
};

const findAllByDate = async (date, startTime, endTime) => {
  replaceDate(startTime, date);
  replaceDate(endTime, date);
  return await Appointment.aggregate([
    {
      $match: {
        date: new Date(date),
      },
    },
    {
      $sort: {
        "services.startTime": 1,
      },
    },
    {
      $project: {
        services: {
          $filter: {
            input: "$services",
            as: "item",
            cond: {
              $and: [
                { $gte: [endTime, "$$item.startTime"] },
                { $lte: [startTime, "$$item.endTime"] },
              ],
            },
          },
        },
      },
    },
  ]);
};

const onlyMathedServices = (appointments, employeeId) => {
  appointments.forEach((a) => {
    a.services = a.services.filter((s) => {
      return s.employee == employeeId;
    });
  });
};

const validateAppointmentDate = ({ date, startTime }) => {
  const validator = new ValidationError();
  if (isBefore(date, new Date()) || isToday(date)) {
    validator.addPath({
      key: "date",
      message: "La date de rendez-vous devrait etre future",
    });
  }
  if (
    !isWithinInterval(parseTime(startTime), {
      start: parseTime("07:00"),
      end: parseTime("18:00"),
    })
  ) {
    validator.addPath({
      key: "startTime",
      message: "L'heure de rendez-vous devra entre 08:00 et 18:00 heure",
    });
  }
  validator.validate();
};

const getAppointmentsPerDay = async (year, month) => {
  const startOfMonth = new Date(year, month - 1, 1);
  const endOfMonth = new Date(year, month, 0, 23, 59, 59, 999);

  const appointmentsPerDay = await Appointment.aggregate([
    {
      $match: {
        date: {
          $gte: startOfMonth,
          $lte: endOfMonth,
        },
      },
    },
    {
      $group: {
        _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
        count: { $sum: 1 },
      },
    },
    {
      $sort: {
        _id: 1,
      },
    },
  ]);

  return appointmentsPerDay;
};

const getAppointmentsPerMonth = async (year) => {
  const appointmentsPerMonth = await Appointment.aggregate([
    {
      $match: {
        date: {
          $gte: new Date(year, 0, 1),
          $lte: new Date(year, 11, 31, 23, 59, 59, 999),
        },
      },
    },
    {
      $group: {
        _id: { $dateToString: { format: "%Y-%m", date: "$date" } },
        count: { $sum: 1 },
      },
    },
    {
      $sort: {
        _id: 1,
      },
    },
  ]);

  return appointmentsPerMonth;
};

const getRevenuesPerDay = async (year, month) => {
  const startOfMonth = new Date(year, month - 1, 1);
  const endOfMonth = new Date(year, month, 0, 23, 59, 59, 999);

  const revenuesPerDay = await Appointment.aggregate([
    {
      $match: {
        date: {
          $gte: startOfMonth,
          $lte: endOfMonth,
        },
      },
    },
    {
      $unwind: "$services",
    },
    {
      $group: {
        _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
        count: { $sum: "$services.service.price.amount" },
      },
    },
    {
      $sort: {
        _id: 1,
      },
    },
  ]);

  return revenuesPerDay;
};

const getRevenuesPerMonth = async (year) => {
  const revenuesPerMonth = await Appointment.aggregate([
    {
      $match: {
        date: {
          $gte: new Date(year, 0, 1),
          $lte: new Date(year, 11, 31, 23, 59, 59, 999),
        },
      },
    },
    {
      $unwind: "$services",
    },
    {
      $group: {
        _id: { $dateToString: { format: "%Y-%m", date: "$date" } },
        count: { $sum: "$services.service.price.amount" },
      },
    },
    {
      $sort: {
        _id: 1,
      },
    },
  ]);

  return revenuesPerMonth;
};

const getPastCommissionPerDay = async () => {
  const endDate = new Date();

  const commissionsPerDay = await Appointment.aggregate([
    {
      $match: {
        date: {
          $lte: endDate,
        },
      },
    },
    {
      $unwind: "$services",
    },
    {
      $group: {
        _id: {
          $dateToString: { format: "%Y-%m-%d", date: "$date" },
        },
        totalCommission: {
          $sum: {
            $divide: [
              {
                $multiply: [
                  "$services.service.price.amount",
                  "$services.service.commission",
                ],
              },
              100,
            ],
          },
        },
      },
    },
    {
      $sort: {
        _id: 1,
      },
    },
  ]);

  const commissions = commissionsPerDay.map((commission) => ({
    name: "Commission",
    price: {
      amount: commission.totalCommission,
      currency: "Ar",
    },
    date: new Date(commission._id),
  }));

  return commissions;
};

const getCommissionPerMonth = async (year) => {
  const startOfYear = new Date(year, 0, 1);
  const endOfYear = new Date(year, 11, 31, 23, 59, 59, 999);

  const commissionsPerMonth = await Appointment.aggregate([
    {
      $match: {
        date: {
          $gte: startOfYear,
          $lte: endOfYear,
        },
      },
    },
    {
      $unwind: "$services",
    },
    {
      $group: {
        _id: { $dateToString: { format: "%Y-%m", date: "$date" } },
        count: {
          $sum: {
            $multiply: [
              {
                $divide: [
                  {
                    $multiply: [
                      "$services.service.price.amount",
                      "$services.service.commission",
                    ],
                  },
                  100,
                ],
              },
              -1,
            ],
          },
        },
      },
    },
    {
      $sort: {
        _id: 1,
      },
    },
  ]);

  return commissionsPerMonth;
};

const appointmentsWithinRange = async (currentDate, hours) => {
  const appointments = await Appointment.find({
    $and: [
      { date: { $gte: currentDate, $lte: hours } },
      { reminderSent: false },
    ],
  }).populate("customer");

  return appointments;
};

const getEmpWorkTimePerWeek = async (year, month) => {
  const startOfMonth = new Date(year, month - 1, 1);
  const endOfMonth = new Date(year, month, 0, 23, 59, 59, 999);

  const allEmployees = await User.find({ roles: "EMP" });

  const durationByEmployee = await Appointment.aggregate([
    {
      $match: {
        date: {
          $gte: startOfMonth,
          $lte: endOfMonth,
        },
      },
    },
    {
      $unwind: "$services",
    },
    {
      $lookup: {
        from: "users",
        localField: "services.employee",
        foreignField: "_id",
        as: "employee",
      },
    },
    {
      $unwind: "$employee",
    },
    {
      $group: {
        _id: "$services.employee",
        empName: {
          $first: {
            $concat: ["$employee.firstName", " ", "$employee.lastName"],
          },
        },
        empPerWeek: {
          $sum: {
            $divide: [
              { $subtract: ["$services.endTime", "$services.startTime"] },
              1000 * 60 * 60,
            ],
          },
        },
      },
    },
  ]);

  allEmployees.forEach((employee) => {
    const existingRecord = durationByEmployee.find((record) =>
      record._id.equals(employee._id)
    );
    if (!existingRecord) {
      durationByEmployee.push({
        _id: employee._id,
        empName: employee.firstName + " " + employee.lastName,
        empPerWeek: 0,
      });
    }
  });

  return durationByEmployee;
};

const getAllMonthsRevenue = async (year) => {
  const revenuesPerMonth = await getRevenuesPerMonth(year);
  const months = Array.from({ length: 12 }, (_, index) => new Date(year, index, 1));
  const monthStrings = months.map(date => date.toISOString().slice(0, 7));
  const mergedData = monthStrings.map(month => {
    const found = revenuesPerMonth.find(item => item._id === month);
    return found ? found.count / 1000 : 0;
  });
  return mergedData;
};

const AppointmentService = {
  makeAppointment,
  validateAppointmentDate,
  findAllByCustomer,
  findAllByEmployee,
  findAllUnassigned,
  findAllByDate,
  getAppointmentsPerDay,
  getAppointmentsPerMonth,
  getRevenuesPerDay,
  getRevenuesPerMonth,
  appointmentsWithinRange,
  getPastCommissionPerDay,
  getCommissionPerMonth,
  getEmpWorkTimePerWeek,
  getAllMonthsRevenue,
};

module.exports = AppointmentService;
