const SpecialOffer = require("../models/special-offer.model");
const { currentDate } = require("../utils/date.utils");
const User = require("../models/user.model");
const NotificationService = require("./notification.service");
const mongoose = require("mongoose");
const Service = require("../models/service.model");

const createSpecialOffer = async (service_id, startDate, endDate, price) => {
  const service = await Service.findById(service_id);
  const newSpecialOffer = new SpecialOffer({
    service: service,
    startDate: startDate,
    endDate: endDate,
    price: price,
  });
  const special = await newSpecialOffer.save();
  service.promotion = special._id;
  await service.save();
  return special;
};

const getCurrentSpecialOffers = async () => {
  const specialOffers = await SpecialOffer.find({
    startDate: { $lte: currentDate() },
    endDate: { $gte: currentDate() },
  });
  return specialOffers;
};

const specialOffersNotification = async (specialOffer) => {
  const roles = ["CUSTOMER"];
  const customers = await User.find({
    roles: roles,
    "preferences.notification": true,
  });
  for (const customer of customers) {
    await NotificationService.notificationEmailSpecialOffer(
      customer.email,
      customer.name,
      specialOffer
    );
  }
};

const SpecialOfferService = {
  getCurrentSpecialOffers,
  specialOffersNotification,
  createSpecialOffer,
};

module.exports = SpecialOfferService;
