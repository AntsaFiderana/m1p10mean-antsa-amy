const { UniqueValidationError } = require("../error/validation.error");
const User = require("../models/user.model");
const jwt = require("jsonwebtoken");
const EmailService = require("./email.service");

const authenticate = async ({ identifier, password }) => {
  const user = await User.findByIdentifier({ identifier, password });
  if (!user) {
    throw new UniqueValidationError("Vos identifiants sont invalides");
  }
  return {
    token: jwt.sign({ user }, process.env.SECRET_KEY, { expiresIn: "2d" }),
    user,
  };
};

const register = async (userData) => {
  const { username, email, password, firstName, lastName } = userData;
  const roles = ["CUSTOMER"];
  const confirmationCode = await generateConfirmationCode();

  const existingUser = await User.findOne({ $or: [{ username }, { email }] });
  if (existingUser) {
    throw new Error(
      "Le nom d'utilisateur et/ou l'email est déjà utilisé dans notre site"
    );
  }

  try {
    await EmailService.sendConfirmationEmail(email, confirmationCode);
  } catch (error) {
    throw new Error("Erreur lors de l'envoie du code de confirmation");
  }

  const newUser = new User({
    username,
    email,
    password,
    firstName,
    lastName,
    roles,
    confirmationCode,
  });

  await newUser.save();

  return newUser;
};

const generateConfirmationCode = async () => {
  const code = Math.floor(Math.random() * 900000) + 100000;
  return code.toString();
};

const confirmCode = async ({ email, confirmationCode }) => {
  const user = await User.findOne({ email, confirmationCode });
  if (!user) {
    return false;
  }

  user.isConfirmed = true;
  await user.save();

  return user;
};

const checkIfUserExist = async ({ username, email }) => {
  const existingUser = await User.findOne({ $or: [{ username }, { email }] });
  if (existingUser) {
    throw new UniqueValidationError(
      "Le nom d'utilisateur et/ou l'email est déjà utilisé dans notre site"
    );
  }
};

const SecurityService = {
  authenticate,
  register,
  confirmCode,
  checkIfUserExist,
};

module.exports = SecurityService;
