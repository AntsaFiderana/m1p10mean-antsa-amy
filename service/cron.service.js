const cron = require("node-cron");
const NotificationService = require("./notification.service");

const hourlyCron = async () => {
  cron.schedule("0 * * * *", async () => {
    NotificationService.appointmentReminder();
  });
};

const CronService = {
  hourlyCron,
};

module.exports = CronService;
