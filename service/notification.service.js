const AppointmentService = require("./appointment.service");
const EmailService = require("./email.service");
const { currentDate, dateAfterHours } = require("../utils/date.utils");

const hoursBeforeAppointment = 6;

const notificationEmailSubject = "Beauty Salon - Appointment Reminder";

const notificationEmailSpecialOfferSubject = "Beauty Salon - Special Offer";

const notificationTemplate = "notification";

const notificationTemplateSpecialOffer = "special-offer";

const appointmentReminder = async () => {
  console.log("-- Appointment Reminder Cron --");
  const cDate = currentDate();
  const range = dateAfterHours(cDate, hoursBeforeAppointment);
  const appointments = await AppointmentService.appointmentsWithinRange(
    cDate,
    range
  );
  if (appointments && appointments.length > 0) {
    await Promise.all(
      appointments.map(async (a) => {
        if (a.customer.preferences.rappel) {
          notificationEmail(
            a.customer.email,
            a.customer.username,
            a.services,
            a.date
          );
          a.reminderSent = true;
          await a.save();
        }
      })
    );
  } else {
    console.log("No appointments found within the specified range.");
  }
};

const notificationEmail = async (
  customerEmail,
  customerName,
  services,
  dateTime
) => {
  const data = {
    customerName: customerName,
    services: services,
    dateTime: dateTime,
  };

  EmailService.sendEmail(
    customerEmail,
    notificationEmailSubject,
    notificationTemplate,
    data
  );
};

const notificationEmailSpecialOffer = async (
  customerEmail,
  customerName,
  specialOffer
) => {
  const data = {
    customerName: customerName,
    specialOffer: specialOffer,
  };

  EmailService.sendEmail(
    customerEmail,
    notificationEmailSpecialOfferSubject,
    notificationTemplateSpecialOffer,
    data
  );
};

const NotificationService = {
  appointmentReminder,
  notificationEmailSpecialOffer,
};

module.exports = NotificationService;
