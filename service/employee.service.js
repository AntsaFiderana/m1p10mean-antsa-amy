const { isBefore, isAfter, startOfDay, endOfDay } = require("date-fns");
const AppointmentService = require("./appointment.service");
const Employee = require("../models/employee.model");
const { getReferenceDate } = require("../utils/date.utils");
const { checkIfUserExist } = require("./security.service");

const defaultWorkingHour = [
  {
    day: 1,
    startTime: getReferenceDate() + "T08:00",
    endTime: getReferenceDate() + "T17:00",
  },
  {
    day: 2,
    startTime: getReferenceDate() + "T08:00",
    endTime: getReferenceDate() + "T17:00",
  },
  {
    day: 3,
    startTime: getReferenceDate() + "T08:00",
    endTime: getReferenceDate() + "T17:00",
  },

  {
    day: 4,
    startTime: getReferenceDate() + "T08:00",
    endTime: getReferenceDate() + "T17:00",
  },
  {
    day: 5,
    startTime: getReferenceDate() + "T08:00",
    endTime: getReferenceDate() + "T17:00",
  },
];

const getDailyTasksByEmployee = async (id, date) => {
  const appointments = await AppointmentService.findAllByEmployee(id, {
    date: {
      $gte: startOfDay(date),
      $lte: endOfDay(date),
    },
  });
  let commission = 0;
  const tasks = [];
  appointments.forEach((appointment) => {
    appointment.services.forEach((item) => {
      commission += item.service.commissionAmount;
      const task = {
        item,
        appointment: appointment._id,
        customer: appointment.customer,
      };
      evaluateTaskStatus(task, new Date());
      tasks.push(task);
    });
  });
  return {
    tasks: tasks.sort((a, b) => a.item.startTime - b.item.startTime),
    commission: commission,
  };
};

const evaluateTaskStatus = (task, currentDate) => {
  const startTime = task.item.startTime;
  const endTime = task.item.endTime;
  if (isBefore(endTime, currentDate)) {
    task.status = "Terminée";
  } else if (isAfter(startTime, currentDate)) {
    task.status = "Prochainement";
  } else {
    task.status = "En cours";
  }
};

const createStaff = async (data) => {
  if (data.roles.includes("EMP")) {
    const newStaff = new Employee(data);
    await checkIfUserExist(newStaff);
    newStaff.workingHours = defaultWorkingHour;
    await newStaff.save();
  } else {
    const newStaff = new User(data);
    await newStaff.save();
  }
};

const EmployeeService = {
  evaluateTaskStatus,
  getDailyTasksByEmployee,
  createStaff,
};

module.exports = EmployeeService;
