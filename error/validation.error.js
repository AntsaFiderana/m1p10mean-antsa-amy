class ValidationError extends Error {
  #paths = new Map();

  constructor() {
    super();
  }

  validate() {
    if (this.#paths.size != 0) throw this;
  }

  addPath({ key, message }) {
    if (this.#paths.has(key)) {
      this.#paths.get(key).push(message);
    } else {
      this.#paths.set(key, [message]);
    }
  }

  get paths() {
    const res = Object.fromEntries(Array.from(this.#paths));
    return res;
  }
}

class UniqueValidationError extends Error {
  error = {};

  constructor(message) {
    super();
    this.error.message = message;
  }
}

const handle = (error, res) => {
  if (error instanceof UniqueValidationError) {
    res.statusCode = 401;
    res.json(error.error);
  } else {
    throw error;
  }
};

module.exports = { ValidationError, UniqueValidationError, handle };
