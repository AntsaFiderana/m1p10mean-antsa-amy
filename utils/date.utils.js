const { parse, format } = require("date-fns");

const getDayOfWeek = (date) => {
  const dateObject = new Date(date);
  return dateObject.getDay();
};

const parseTime = (time, date = new Date()) => parse(time, "HH:mm", date);

const getReferenceDate = () => "2024-02-14";

const replaceDate = (myDate, currentDate) => {
  const dateFormated = new Date(currentDate);
  myDate.setFullYear(dateFormated.getFullYear());
  myDate.setMonth(dateFormated.getMonth());
  myDate.setDate(dateFormated.getDate());
};

const currentDate = () => new Date();

const dateAfterHours = (cDate, hours) => {
  return new Date(cDate.getTime() + hours * 60 * 60 * 1000);
};

const readableDate = (date) => {
  return format(date, "iiii d MMMM yyyy 'at' HH:mm");
}

const readableTime = (date) => {
  return format(date, "HH:mm");
}

module.exports = {
  getDayOfWeek,
  parseTime,
  getReferenceDate,
  replaceDate,
  currentDate,
  dateAfterHours,
  readableDate,
  readableTime
};
